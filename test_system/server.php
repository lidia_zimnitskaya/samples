<?php

declare(strict_types=1);

session_start();

require_once('config.php');

// Autoload classes.
function __autoload(string $classname) : void
{
    require_once('class/' . $classname . '.class.php');
}

//Create objects
$DB= new DBdata();
$UserRight = new UserRights();
$newTpl = new Template();
$question = new Question();
$test = new Test();

if(isset($_POST['queryType']) && $_POST['queryType']== 'changeClass') {
    $UserRight -> changeUserClass($_POST['u_login'], $_POST['newClass']);
}
elseif (isset($_POST['queryType']) && $_POST['queryType']== 'insertQuestion'){
    $question -> insertQuestion($_POST, $_FILES);
}
elseif (isset($_POST['queryType']) && $_POST['queryType']== 'insertTest'){
    $test->insertTest($_POST);
}

if(isset($_POST['tpl'])) {
    $tpl = $_POST['tpl'];
}

if(isset($_POST['queryType']) && $_POST['queryType']== 'getTemplete') {
    $newTpl->setLabels($DB->getAllLabels());
    $newTpl->setUserRights($UserRight->getAllUsers());
    $newTpl->setUserClasses($UserRight->getAllUserClasses());
    $newTpl->setTestCategories($test->getTestCategories());
    $newTpl->setTestQuestions($test->getTestQuestions());
    $newTpl->readMainTemplate($tpl);
    $newTpl->proccess_tpl_files();
    echo $newTpl->get_file_contents();
}