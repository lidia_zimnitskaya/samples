<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/ajax.js"></script>
<div class="section main_section">
    <div class="row work_area">
        <div class="col col-1-5" style="margin-left: 20px">
            <h2>Choose a test</h2>
            <ul id="menu" style="list-style-type: none;padding-right: 0px; padding-left: 0px;">
                {LOOP=test_names="user_menu_test_list.tpl"}
            </ul>
        </div>