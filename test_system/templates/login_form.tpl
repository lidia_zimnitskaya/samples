
<div class="section main_section">
    <div class="row work_area">
        <div class="col col-1-5">

        </div>
        <div class="col col-3-5 vertical_border">


            <form action="" method="post" enctype="">
            <div class="table">
			    <div class="tr">
                    <div class="td_text">                        
                    </div>
                    <div class="td" style="color:red;font-weight:bold;">
						{DV="error_massage"}
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">
                        {LABEL="text_login"}:
                    </div>
                    <div class="td">
                        <input type="text" class="input" name="user" placeholder="" value="">
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">
                        {LABEL="text_password"}:
                    </div>
                    <div class="td">
                        <input type="password" class="input" name="password" placeholder="" value="">
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">
                        {LABEL="text_remember"}:
                    </div>
                    <div class="td">
                        <div class="checkbox">
                            <input type="checkbox" id="login-check" name="remember">
                            <label for="login-check"></label>
                        </div >
                    </div>
                </div>
                <div class="tr">
                    <div class="td_text">

                    </div>
                    <div class="td">
                        <a href="/php04_zimnitskaya/megatask/index.php"><input type="submit" class="button" name="submit" value="{LABEL="button_login"}" class="" /></a>
                    </div>
                </div>
				<div class="tr">
                    <div class="td_text">
                        
                    </div>
                    <div class="td"  style="font-size: 80%; color: black;">
						
                        {LABEL="text_not_registered"} <a href="?registered=false">{LABEL="button_not_registered"}</a>    
                    </div>
                </div>
            </div>
			
        </form>
		
            <div style="clear:both;"></div>

        </div>
        <div class="col col-1-5">
            <div class="information">
                
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>