<?php


declare(strict_types=1);

session_start();

require_once('config.php');

// Autoload classes.
function __autoload(string $classname) : void
{
	require_once('class/' . $classname . '.class.php');
}

//Create objects
$DB_data = new DBdata();
$user = new User();
$u_test = new Test();
$template = new Template();
$template -> setLabels($DB_data -> getAllLabels());
$template -> setDynamicVars($DB_data -> getAllMessages());
$template->setTestNames($u_test->getTestNames());
$template -> setDymanicVar('error_massage', '');

// defining what template to handle
if ($user -> is_loggedin() == true) {
	$template -> setUserData($user -> getUserData());
	if($user -> getUserData()['u_type']==="1"){
		$template -> readMainTemplate('admin.tpl');
	}
	else {
		$template -> readMainTemplate('after_login.tpl');
	}
}
else {
	$template -> readMainTemplate('before_login.tpl');
}

// logging in
if(isset($_POST['submit'])){
	$responce=$user->login($_POST['user'], $_POST['password']);
	if($responce===true){
		$template -> setUserData($user -> getUserData());
		if($user -> getUserData()['u_type']==="1"){
			$template -> readMainTemplate('admin.tpl');
		}
		else {
			$template -> readMainTemplate('after_login.tpl');
		}		
	}
	else {
		$template -> setDymanicVar('error_massage', $DB_data -> getAllMessages()['wrong_login_or_password']);
	}
}

//registering
if (isset($_POST['register'])) {

	$responce=$user->register($_POST['name'], $_POST['email'], $_POST['user'], $_POST['password']);
	if(isset($responce[1])==false){
		$template -> readMainTemplate('after_login.tpl');
	}
	else if(isset($responce[1]) && $responce[1]==1062) {
		$template -> setDymanicVar('error_massage', $DB_data -> getAllMessages()['login_already_exist']);
	}
	else if(isset($responce[1])) {
		$template -> setDymanicVar('error_massage', 'Error.');
	}
}

//logging out
if(isset($_GET['logout']) && $_GET['logout']==true) {
	$user->logout();
	$template -> readMainTemplate('before_login.tpl');
}
if(isset($_GET['registered'])) {
	if($_GET['registered']=='true') {
		$template -> readMainTemplate('before_login.tpl');
	}
	else if ($_GET['registered']=='false') {
		$template -> readMainTemplate('register.tpl');
	}
}
$user->logout();
$template -> proccess_tpl_files();
echo $template -> get_file_contents();