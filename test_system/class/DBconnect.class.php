<?php

declare(strict_types=1);

class DBconnect
{
	private $connection = null;
	
	// Use MySQL::getInstance() to get an object.
	public static function getInstance() : DBconnect
    {
        static $instance = null;
        if (is_null($instance)) {
            $instance = new DBconnect();
			$instance->setConnection();
        }
        return $instance;
    }

	// This method is called from getInstance() to establish MySQL connection.
	private function setConnection() : void
	{
		$this->connection = new PDO('mysql:dbname='.CONFIG_DB_BASE.';host='.CONFIG_DB_HOST.';charset='.CONFIG_DB_CHAR, CONFIG_DB_USER, CONFIG_DB_PASS);
	}
	
	public function runQuery(string $query, array $parameters=array()) : PDOStatement 
	{
		try{
			$stmt = $this->connection->prepare($query);
			foreach ($parameters as $key=>$values) {
				$stmt->bindValue($key, $values);
			}
			$stmt->execute();
			return $stmt;
		}
		catch(PDOException $e)
       {
           echo $e->getMessage();
       }
	}
}