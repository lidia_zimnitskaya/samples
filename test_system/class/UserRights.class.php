<?php

declare(strict_types=1);

class UserRights
{
    private $classes = array();
    private $users = array();

    public function __construct()
    {
        $mysql = DBconnect::getInstance();

        $r = $mysql->runQuery("SELECT * FROM `user_classes`");
        $this->classes = $r->fetchAll(PDO::FETCH_ASSOC);

        $r = $mysql->runQuery("SELECT `user`.`u_login`, `u_name`, `user_classes`.`cl_name` FROM `user` INNER JOIN `user_classes` ON `user`.`u_type`=`user_classes`.`cl_id`");
        $this->array = $r->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllUserClasses () : array
    {
        return $this->classes;
    }

    public function getAllUsers () : array
    {
        foreach ($this->array as $value) {
            $value['table_text_action']="Change";
            $this->users[]=$value;
        }
        return $this->users;
    }

    public function changeUserClass(string $login, string $newClass) : bool
    {
        $mysql = DBconnect::getInstance();
        $stmt = $mysql->runQuery("SELECT `cl_id` FROM `user_classes` WHERE `cl_name`=:type", array('type'=>$newClass));
        $classID = $stmt->fetch(PDO::FETCH_ASSOC);
        $mysql->runQuery("UPDATE `user` SET `u_type`=:type WHERE `u_login`=:login", array(':type'=>$classID['cl_id'], ':login'=>$login));
        return true;
    }
}



