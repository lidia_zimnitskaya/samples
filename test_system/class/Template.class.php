<?php

class Template
{
    private $page;
    private $templates_dir = "templates/";
	private $labels = array();
	private $messages = array();
	private $user_data = array();
	private $user_rights = array();
    private $user_classes = array();
    private $categories = array();
    private $test_questions = array();
    private $test_names = array();


    function __construct()
    {

    }

    public function readMainTemplate($tpl_file) : string
    {
        $tpl_full_path = $this->templates_dir.$tpl_file;
        if (!is_file($tpl_full_path)) {
            throw new Exception('Main template [' . $tpl_file . '] not found!');
        }
        else {
			$this->page = file_get_contents($tpl_full_path);          
        }
		return $this->page;
    }
	
	public function readTemlatesFile($tpl_file) 
	{
		$tpl_full_path = $this->templates_dir.$tpl_file[1];
        if (!is_file($tpl_full_path)) {
            throw new Exception('Main template [' . $tpl_file . '] not found!');
        }
        else {
            return file_get_contents($tpl_full_path);
        }
	}
	
	public function  setLabels(array $labels_arr) :void
	{
		$this->labels = $labels_arr;
	}

    public function processLabelVars(array $var) : string
    {
        $db_var = $var[1];
        return $this->labels[$db_var];
    }

    public function setDynamicVars(array $dynamic_arr) :void
	{
		$this->messages = $dynamic_arr;
	}

    public function processDymanicVars(array $var)  :?string
    {
        $message_text = $var[1];
        if (isset($this->messages[$message_text])) {
            return $this->messages[$message_text];
        }
        else {
            return null;
        }
    }
    public function setDymanicVar(string $name, string $text) : void
    {
        $this->messages[$name] = $text;
    }

	public function setUserData(array $user_arr=array()) :void
	{
		$this->user_data = $user_arr;
	}

    public function processUserData(array $var) : string
    {
        $db_var = $var[1];
        return $this->user_data[$db_var];
    }

	public function getCopyrightDate(array $var) :string
	{
		if ($this->labels['start_year'] != date('Y')){
			$copyright = $this->labels['start_year']." - ".date('Y');
		}
		else {
			$copyright = $this->labels['start_year'];
		}
		return $copyright;
	}

    public function setUserRights(array $user_arr=array()) :void
    {
        $this->user_rights = $user_arr;
    }

    public function setUserClasses(array $class_arr=array()) :void
    {
        $this->user_classes = $class_arr;
    }

    public function setTestCategories(array $category_arr=array()) :void
    {
        $this->categories = $category_arr;
    }

    public function setTestQuestions(array $question_arr=array()) :void
    {
        $this->test_questions = $question_arr;
    }

    public function setTestNames(array $names_arr=array()) :void
    {
        $this->test_names = $names_arr;
    }

    public function proccessLoops($var) : string
    {
        $db_var = $var[1];  // getting the name of an array which contains the necessary data. We put that name in the placeholder {LOOP=user_permits="files_permissions.tpl"}
        $tpl_file = $this->templates_dir.$var[2];  // the path where the template is. ALso take it from the placeholder.
        $count = count($this->$db_var); // count how many times we need to upload the loop template
        $text = "";
        for ($i=0; $i<$count; $i++){   // start the loop which will handle the ultimate template
            $text .= file_get_contents($tpl_file);  // get content from the template
            $pattern = "/{LD=\"([\w\.\_]+)\"}/";
            preg_match_all ($pattern, $text, $matches); // looking for the matches first and i+ times
            $count_m = count($matches[1]);  // count how many matches (vars) we get each time
            for ($j=0; $j<$count_m; $j++) { // start a new loop to replace each placeholder with the an appropriate array data
                $v = $matches[1][$j];
                $replacement = $this->$db_var[$i][$v];
                $text = preg_replace("/{LD=\"([\w\.\_]+)\"}/", $replacement  , $text, $limit = 1);  // replace just one match in a time
            }
        }
        return $text;
    }

    public function proccess_tpl_files() :void
    {
        while (preg_match("/{FILE=\"[\w\.\_]+\"}|{LOOP=([\w\.\_]+)=\"([\w\.\_]+)\"}/", $this->page)) {
            $this->page = preg_replace_callback("/{FILE=\"([\w\.\_]+)\"}/", array($this, 'readTemlatesFile'), $this->page);
            $this->page = preg_replace_callback("/{LOOP=([\w\.\_]+)=\"([\w\.\_]+)\"}/", array($this, 'proccessLoops'), $this->page);
        }
        $this->page = preg_replace_callback("/{LABEL=\"([\w\.\_]+)\"}/", array($this, 'processLabelVars'), $this->page);
        $this->page = preg_replace_callback("/{COPYRIGHT=\"([\w\.\_]+)\"}/", array($this, 'getCopyrightDate'), $this->page);
        $this->page = preg_replace_callback("/{DV=\"([\w\.\_]+)\"}/", array($this, 'processDymanicVars'), $this->page);
        $this->page = preg_replace_callback("/{UDATA=\"([\w\.\_]+)\"}/", array($this, 'processUserData'), $this->page);

    }

    public function get_file_contents() : string
    {
       return $this->page;
    }
}
