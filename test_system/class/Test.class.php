<?php

declare(strict_types=1);

class Test
{
    private $test_categories = array();
    private $test_questions = array();
    private $test_names = array();

    public function __construct()
    {
        $mysql = DBconnect::getInstance();

        $r = $mysql->runQuery("SELECT * FROM `test_categories` ORDER BY `category_id` ASC");
        $this->test_categories = $r->fetchAll(PDO::FETCH_ASSOC);

        $r = $mysql->runQuery("SELECT * FROM `question`");
        $this->array = $r->fetchAll(PDO::FETCH_ASSOC);

        $r = $mysql->runQuery("SELECT `test`.`t_id`, `test`.`t_name`, `test`.`time`, `test`.`category_id`, `test_categories`.`category_name` FROM `test` INNER JOIN `test_categories` ON   `test`.`category_id`=`test_categories`.`category_id`");
        $this->test_names = $r->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTestCategories () : array
    {
        return $this->test_categories;
    }

    public function getTestQuestions () : array
    {
        $i=1;
        foreach ($this->array as $value) {
            $value['counter'] = $i++;
            $this->test_questions[]=$value;
        }
        return $this->test_questions;
    }

    public function getTestNames () : array
    {
        return $this->test_names;
    }

    public function insertTest(array $test_data)
    {
        $mysql = DBconnect::getInstance();
        $mysql->runQuery("INSERT INTO `test`(`t_name`, `category_id`, `time`) VALUES (:t_name, :category_id, :time)",
                        array(':t_name'=>$_POST['testName'], ':category_id'=>$_POST['testCategoryId'], ':time'=>$_POST['timeLimit']));

        //getting the id of the test
        $stmt = $mysql->runQuery("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();

        //inserting the test questions
        for($i=1; $i<=$test_data['quantity']; $i++) {
            if(isset($test_data['questionId'.$i])) {
                $mysql->runQuery("INSERT INTO `test_questions`(`tq_question_id`, `tq_test_id`) VALUES (:tq_question_id, :tq_test_id)",
                                array(':tq_question_id'=>$_POST['questionId'.$i], ':tq_test_id'=>$lastId));
            }
        }
    }
}