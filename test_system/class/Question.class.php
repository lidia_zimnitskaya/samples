<?php

declare(strict_types=1);

class Question
{

    public function insertQuestion(array $data_arr, array $files_arr=array()) :void
    {
        //counting how many right answers the question has to define its type
        $counter=0;
        for($i=1; $i<=5; $i++) {
            if($data_arr['answer'.$i.'_right']==='true') {
                $counter++;
            }
        }
        if($counter=== 1) {
            $q_class=1; //single answer
        }
        elseif ($counter > 1) {
            $q_class=2; //multiple answer
        }

        //if the file is not attached
        if(!isset($files_arr['file']['name'])) {
            $files_arr['file']['name']='white.jpg';
        }
        // else uploading the file
        else {
            $fileName = $_FILES['file']['name']; // setting the value for $filename to use it in check_file() method
            $uploaddir = '/var/www/php04_zimnitskaya/megatask/img/';
            $uploadfile_file= $uploaddir . $fileName;

            if (file_exists($uploadfile_file) != true) {
                if (file_exists($uploaddir) == false) { // checking if the directory for a user is exist
                    mkdir($uploaddir);
                }
                //moving file to user folder
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile_file)) {
                    //echo "File was successfully uploaded.\n";  //plannig to put config messages one day
                } else {
                    // echo "Something went wront. Please try again.";
                }
            }
        }

        $mysql = DBconnect::getInstance();
        $mysql->runQuery("INSERT INTO `question`(`q_text`, `q_type`, `q_picture`) VALUES (:text, :class, :picture)",
                                array(':text'=>$_POST['question_text'], ':class'=>$q_class, ':picture'=>$files_arr['file']['name']));

        //getting the id of the question
        $stmt = $mysql->runQuery("SELECT LAST_INSERT_ID()");
        $lastId = $stmt->fetchColumn();

        // inserting the answers for question
        for ($j=1; $j<=5; $j++) {
            if(isset($data_arr['answer'.$j])){
                $mysql->runQuery("INSERT INTO `answer`(`a_text`) VALUES (:a_text)",
                                 array(':a_text'=>$data_arr['answer'.$j]));
                $stmt = $mysql->runQuery("SELECT `a_id` FROM `answer` WHERE `a_text`=:a_text",
                                        array(':a_text'=>$data_arr['answer'.$j]));
                $id_arr = $stmt->fetch(PDO::FETCH_ASSOC);
                if($data_arr['answer'.$j.'_right']==='true'){
                    $qa_right_or_not=1;
                }
                else {
                    $qa_right_or_not=0;
                }
                $mysql->runQuery("INSERT INTO `question_answer`(`qa_question_id`, `qa_answer_id`, `qa_right_or_not`) VALUES (:qa_question_id, :qa_answer_id, :qa_right_or_not)",
                                array(':qa_question_id'=>$lastId, ':qa_answer_id'=>$id_arr['a_id'], ':qa_right_or_not'=>$qa_right_or_not));

            }
        }
    }
}
