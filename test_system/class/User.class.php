<?php


Class USER {

    private $db = false;
	private $user_data = array();

    function __construct()
    {
        $this->db = DBconnect::getInstance();
    }

    public function login(string $uname, string $upassword) :bool
    {
		$uname = $_POST['user'] ?? ''; //checking that the inputs not empty
		$upassword = $_POST['password'] ?? '';

		$uname = trim($uname);
		$upassword = trim($upassword);
		if (mb_strlen($uname)>0 && mb_strlen($upassword)>0){
			$stmt = $this->db->runQuery("SELECT * FROM user WHERE u_login =:login AND u_password =:password", array(':login'=>$uname, ':password'=> sha1($upassword)));      
			if($stmt->rowCount() === 1) {
				$userData=$stmt->fetch(PDO::FETCH_ASSOC);
				$_SESSION['user_session'] = $userData['u_id']; // setting $_sesssion to get the file catalog

				if (isset($_POST['remember']) && $_POST['remember']=='on') {
					
					$remember_hash = sha1($userData['u_login'].$_SERVER['REMOTE_ADDR']);
					$date = time()+1209600;
					$cookie = setcookie("Remember", $userData['u_login'], $date);// setting $_cookie to make a long-time authorization
					$this->db->runQuery("UPDATE `user` SET `u_remember`=:remember WHERE `u_id`=:id", 
										array(':remember'=>$remember_hash, ':id'=>$userData['u_id']));
				}
				
				return true;
			}
			else {
				return false;
			}
			
		}
		else {
			return false;
		}
    }

    public function is_loggedin() :bool
    {
		if(isset($_COOKIE['Remember'])) {
			$stmt = $this->db->runQuery("SELECT * FROM `user` WHERE `u_remember`=:remember", 
									array(':remember'=>sha1($_COOKIE['Remember'].$_SERVER['REMOTE_ADDR'])));
			if($stmt->rowCount() === 1) {
				$userCookie=$stmt->fetch(PDO::FETCH_ASSOC);
			}
		}

        if(isset($_SESSION['user_session']) || isset($userCookie['u_remember'])) //checking if the user was logged in before
        {
            return true;
        }
		else {
			return false;
		}
    }
	
	public function register(string $uname, string $umail, string $ulogin, string $upass) //:bool
    {
		$stmt = $this -> db -> runQuery("INSERT INTO `user`(`u_id`, `u_login`, `u_password`, `u_name`, `u_email`, `u_type`) 
										VALUES (:id, :login, :password, :name, :email, :type)",
										array(':id'=>NULL, ':login'=>$ulogin, ':password'=>sha1($upass), ':name'=>$uname, ':email'=>$umail, ':type'=>2));
		$userSession=$stmt->fetch(PDO::FETCH_ASSOC); 
		$_SESSION['user_session'] = $userSession['u_id']; // setting $_sesssion to get the file catalog
		
		return $stmt->errorInfo();
    }

    public function logout() : void
    {
        session_destroy();
		if(isset($_COOKIE['Remember'])) {
			$stmt = $this->db->runQuery("UPDATE `user` SET `u_remember`='' WHERE `u_remember`=:remember",
									array(':remember'=>sha1($_COOKIE['Remember'].$_SERVER['REMOTE_ADDR'])));
			unset($_COOKIE['Remember']);         // unsetting the variables to log out
			setcookie("Remember", "", time()-1209600);						
		}
        if (isset($_SESSION['user_session'])) {
			unset($_SESSION['user_session']);
		}
    }
	
	public function getUserData():?array
	{
		if(isset($_COOKIE['Remember'])) {
			$stmt = $this->db->runQuery("SELECT * FROM `user` WHERE `u_remember`=:remember", 
									array(':remember'=>sha1($_COOKIE['Remember'].$_SERVER['REMOTE_ADDR'])));
			if($stmt->rowCount() === 1) {
				$userCookie=$stmt->fetch(PDO::FETCH_ASSOC);
			}
		}
		if(isset($_SESSION['user_session'])) {
			$stmt = $this->db->runQuery("SELECT * FROM `user` WHERE `u_id`=:id", 
									array(':id'=>$_SESSION['user_session']));
			if($stmt->rowCount() === 1) {
				$userSession=$stmt->fetch(PDO::FETCH_ASSOC);
			}
		}

        if(isset($_SESSION['user_session'])) //checking if the user was logged in before
        {
			return $this->user_data=$userSession;
        }
		else if(isset($userCookie['u_remember'])) //checking if the user was logged in before
        {
			return $this->user_data=$userCookie;
        }
		else {
			return NULL;
		}
	}
}

